#!/usr/bin/env python

import cProfile
from datetime import datetime
import logging
import os
import sys
import time

import eventlet

from oslo.config import cfg
from oslo import messaging
from oslo.messaging import opts

logging.basicConfig(stream=sys.stdout, level=logging.WARNING)

LOG = logging.getLogger(__name__)

total_count = 0
start = time.time()
end = time.time()


class TestEndpoint(object):

    def __init__(self):
        self.buffer = []
        self.buffer_size = 1

    def test(self, ctx, arg):
        self.buffer.append(arg)
	self.flush()

    def flush(self):
	global total_count
        total_count += 1
        LOG.warn('Received %s messages' % len(self.buffer))
        self.buffer = []
        if total_count == 10000:
            end = time.time()
	    with open('res.txt', 'w') as f:
	        f.write("Received {0} messages\n".format(total_count))
		f.write("Estimated speed: {0} msg/s".format(total_count/int(end-start)))                                            


eventlet.monkey_patch()
cfg.CONF(sys.argv[1: ])
transport = messaging.get_transport(cfg.CONF)
target = messaging.Target(topic='testtopic', server='server1', version='1.0')
server = messaging.get_rpc_server(transport, target, [TestEndpoint()],
                                  executor='eventlet')

def start_server():
 
    profiler = cProfile.Profile()
    try:
        profiler.enable()
        server.start()
        server.wait()
    except KeyboardInterrupt:
        profiler.disable()
        dir_name = os.path.dirname(os.path.abspath(__file__))
        file_name = 'server-%s.pstats' % datetime.now().strftime('%d%m_%H:%M:%S')
        profiler.dump_stats(os.path.join(dir_name, file_name))


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    start_server()
