#!/usr/bin/env python

import cProfile
from datetime import datetime
import logging
import sys
import threading
import time

import eventlet
from oslo.config import cfg
from oslo import messaging

from oslo.messaging._drivers import amqpdriver

CLIENTS = 1
CLIENT_MESSAGES = 1000
LOG = logging.getLogger(__name__)

total_count = 0
eventlet.monkey_patch()

class TestClient(threading.Thread):

    def __init__(self, *args, **kwargs):
        super(TestClient, self).__init__(*args, **kwargs)
	cfg.CONF(sys.argv[1: ])                                                       
        self.transport = messaging.get_transport(cfg.CONF)
        target = messaging.Target(topic='testtopic', version='1.0')
        self._client = messaging.RPCClient(self.transport, target)


    def run(self):
        p = eventlet.GreenPool(50)
        for i in range(CLIENT_MESSAGES):
	    p.spawn_n(self.send_message)
	p.waitall()

    def send_message(self):
            self._client.cast({}, 'test', arg=self.name)
	    global total_count
	    total_count += 1


def send_messages(n):
    for i in range(n):
	LOG.warn('Sent message %s' % i)
        t = TestClient()
        t.run()

def start_client():
    eventlet.monkey_patch()
    profiler = cProfile.Profile()
    try:
        profiler.enable()
        send_messages(CLIENTS)
    except KeyboardInterrupt:
        pass
    finally:
        profiler.disable()
        file_name = 'client-%s.pstats' % datetime.now().strftime('%d%m_%H:%M:%S')
        profiler.dump_stats(file_name)


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    p = eventlet.greenpool.GreenPool()
    p.spawn_n(start_client)
    eventlet.hubs.get_hub().run()
